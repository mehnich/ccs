import React, { useState, useEffect } from 'react';
import styled, { ThemeProvider } from 'styled-components';
import { violetTheme, baseTheme } from './Components/Constants';
import GlobalStyles from './Components/GlobalStyles';
import Container from './Components/Container';
import Header from './Components/Header';
import Hero from './Components/Hero';
import Categories from './Components/Categories';
import HeroSecond from './Components/HeroSecond';
import FeaturesList from './Components/FeaturesList';
import Footer from './Components/Footer';
import { ScreenReadersOnly } from './Components/Helpers';

const StickyFooter = styled(Container)`
  flex: 1 0 auto;
`;

function App() {
  const [theme, setTheme] = useState('base');
  const isVioletTheme = theme === 'violet';

  const toggleTheme = () => {
    const updatedTheme = isVioletTheme ? 'base' : 'violet';
    setTheme(updatedTheme);
    localStorage.setItem('theme', updatedTheme);
  };

  useEffect(() => {
    const savedTheme = localStorage.getItem('theme');
    if (savedTheme && ['violet', 'base'].includes(savedTheme)) setTheme(savedTheme);
    else setTheme('violet');
  }, []);

  return (
    <>
      <ThemeProvider theme={isVioletTheme ? violetTheme : baseTheme}>
        <GlobalStyles />
        <StickyFooter>
          <Header />

          <main>
            <ScreenReadersOnly as="h1">Main heading for SEO</ScreenReadersOnly>
            <Hero />
            <Categories toggleTheme={toggleTheme} />
            <HeroSecond />
            <FeaturesList />
          </main>
        </StickyFooter>

        <Container>
          <Footer />
        </Container>
      </ThemeProvider>
    </>
  );
}

export default App;
