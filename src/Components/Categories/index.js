import { ScContainer, ScTopPanel, ScSwiperNavigation } from "./styled";
import Text from '../Text';
import Button from '../Button';
import { Link } from '../Link';
import Icon from '../Icon';

import { Navigation } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import 'swiper/css/navigation';

import Image1 from '../../Img/image-1.jpg';
import Image12x from '../../Img/image-1@2x.jpg';
import Image1Webp from '../../Img/image-1.webp';
import Image12xWebp from '../../Img/image-1@2x.webp';
import Image2 from '../../Img/image-2.jpg';
import Image22x from '../../Img/image-2@2x.jpg';
import Image2Webp from '../../Img/image-2.webp';
import Image22xWebp from '../../Img/image-2@2x.webp';
import Image3 from '../../Img/image-3.jpg';
import Image32x from '../../Img/image-3@2x.jpg';
import Image3Webp from '../../Img/image-3.webp';
import Image32xWebp from '../../Img/image-3@2x.webp';
import Image4 from '../../Img/image-4.jpg';
import Image42x from '../../Img/image-4@2x.jpg';
import Image4Webp from '../../Img/image-4.webp';
import Image42xWebp from '../../Img/image-4@2x.webp';
import Image5 from '../../Img/image-5.jpg';
import Image52x from '../../Img/image-5@2x.jpg';
import Image5Webp from '../../Img/image-5.webp';
import Image52xWebp from '../../Img/image-5@2x.webp';
import Image6 from '../../Img/image-6.jpg';
import Image62x from '../../Img/image-6@2x.jpg';
import Image6Webp from '../../Img/image-6.webp';
import Image62xWebp from '../../Img/image-6@2x.webp';

const Categories = ({ toggleTheme }) => {
  return (
    <ScContainer>
      <ScTopPanel>
        <Text as="h2" className="title" h3>Categories</Text>

        <ScSwiperNavigation>
          <Button className="swiper-button-prev" type="button" aria-label="prev"></Button>
          <Button className="swiper-button-next" type="button" aria-label="next"></Button>
        </ScSwiperNavigation>

        <Button className="button" rounded type="button" onClick={toggleTheme}>
          Live Chat
          <Icon icon="chat" className="icon" width="28" height="26" stroke="#fff" />
        </Button>
      </ScTopPanel>

      <Swiper
        modules={[Navigation]}
        spaceBetween={30}
        slidesPerView={'auto'}
        navigation={{
          prevEl: '.swiper-button-prev',
          nextEl: '.swiper-button-next',
        }}
        speed={500}
        breakpoints={{
          1440: {
            width: 1440,
            spaceBetween: 42,
            slidesPerView: 6,
            slidesPerGroup: 6,
          },
        }}
      >
        <SwiperSlide>
          <Link href="#" className="link" noUnderline>
            <picture>
              <source srcSet={`${Image1Webp}, ${Image12xWebp} 2x`} type="image/webp" />
              <source srcSet={`${Image1}, ${Image12x} 2x`} type="image/jpg" />
              <img className="image" src={Image1} srcSet={`${Image1}, ${Image12x} 2x`} width="197" height="197" alt="category" />  
            </picture>
            
            <Text className="title" bold>Abrasives</Text>
          </Link>
        </SwiperSlide>

        <SwiperSlide>
          <Link href="#" className="link" noUnderline>
            <picture>
              <source srcSet={`${Image2Webp}, ${Image22xWebp} 2x`} type="image/webp" />
              <source srcSet={`${Image2}, ${Image22x} 2x`} type="image/jpg" />
              <img className="image" src={Image2} srcSet={`${Image2}, ${Image22x} 2x`} width="197" height="197" alt="category" />  
            </picture>

            <Text className="title" bold>Electrical</Text>
          </Link>
        </SwiperSlide>

        <SwiperSlide>
          <Link href="#" className="link" noUnderline>
            <picture>
              <source srcSet={`${Image3Webp}, ${Image32xWebp} 2x`} type="image/webp" />
              <source srcSet={`${Image3}, ${Image32x} 2x`} type="image/jpg" />
              <img className="image" src={Image3} srcSet={`${Image3}, ${Image32x} 2x`} width="197" height="197" alt="category" />  
            </picture>

            <Text className="title" bold>Welding</Text>
          </Link>
        </SwiperSlide>

        <SwiperSlide>
          <Link href="#" className="link" noUnderline>
            <picture>
              <source srcSet={`${Image4Webp}, ${Image42xWebp} 2x`} type="image/webp" />
              <source srcSet={`${Image4}, ${Image42x} 2x`} type="image/jpg" />
              <img className="image" src={Image4} srcSet={`${Image4}, ${Image42x} 2x`} width="197" height="197" alt="category" />  
            </picture>

            <Text className="title" bold>Safety</Text>
          </Link>
        </SwiperSlide>

        <SwiperSlide>
          <Link href="#" className="link" noUnderline>
            <picture>
              <source srcSet={`${Image5Webp}, ${Image52xWebp} 2x`} type="image/webp" />
              <source srcSet={`${Image5}, ${Image52x} 2x`} type="image/jpg" />
              <img className="image" src={Image5} srcSet={`${Image5}, ${Image52x} 2x`} width="197" height="197" alt="category" />  
            </picture>

            <Text className="title" bold>Fasteners</Text>
          </Link>
        </SwiperSlide>

        <SwiperSlide>
          <Link href="#" className="link" noUnderline>
            <picture>
              <source srcSet={`${Image6Webp}, ${Image62xWebp} 2x`} type="image/webp" />
              <source srcSet={`${Image6}, ${Image62x} 2x`} type="image/jpg" />
              <img className="image" src={Image6} srcSet={`${Image6}, ${Image62x} 2x`} width="197" height="197" alt="category" />  
            </picture>

            <Text className="title" bold>Hand Tools</Text>
          </Link>
        </SwiperSlide>

        <SwiperSlide>
          <Link href="#" className="link" noUnderline>
            <picture>
              <source srcSet="https://placehold.co/197x197/violet/white.webp, https://placehold.co/197x197/violet/white@2x.webp 2x" type="image/webp" />
              <source srcSet="https://placehold.co/197x197/violet/white.jpg, https://placehold.co/197x197/violet/white@2x.jpg 2x" type="image/jpg" />
              <img className="image" src="https://placehold.co/197x197/violet/white.jpg" srcSet="https://placehold.co/197x197/violet/white.jpg, https://placehold.co/197x197/orange/white@2x.jpg 2x" width="197" height="197" alt="category" />  
            </picture>

            <Text className="title" bold>Hand Tools</Text>
          </Link>
        </SwiperSlide>

        <SwiperSlide>
          <Link href="#" className="link" noUnderline>
            <picture>
              <source srcSet="https://placehold.co/197x197/orange/white.webp, https://placehold.co/197x197/orange/white@2x.webp 2x" type="image/webp" />
              <source srcSet="https://placehold.co/197x197/orange/white.jpg, https://placehold.co/197x197/orange/white@2x.jpg 2x" type="image/jpg" />
              <img className="image" src="https://placehold.co/197x197/orange/white.jpg" srcSet="https://placehold.co/197x197/orange/white.jpg, https://placehold.co/197x197/orange/white@2x.jpg 2x" width="197" height="197" alt="category" />  
            </picture>

            <Text className="title" bold>Electrical</Text>
          </Link>
        </SwiperSlide>

        <SwiperSlide>
          <Link href="#" className="link" noUnderline>
            <picture>
              <source srcSet="https://placehold.co/197x197/pink/white.webp, https://placehold.co/197x197/pink/white@2x.webp 2x" type="image/webp" />
              <source srcSet="https://placehold.co/197x197/pink/white.jpg, https://placehold.co/197x197/pink/white@2x.jpg 2x" type="image/jpg" />
              <img className="image" src="https://placehold.co/197x197/pink/white.jpg" srcSet="https://placehold.co/197x197/pink/white.jpg, https://placehold.co/197x197/orange/white@2x.jpg 2x" width="197" height="197" alt="category" />  
            </picture>

            <Text className="title" bold>Welding</Text>
          </Link>
        </SwiperSlide>
      </Swiper>
    </ScContainer>
  )
}

export default Categories;
