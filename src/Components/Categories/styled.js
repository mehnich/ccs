import styled from 'styled-components';
import { radius, colors, sizes, fonts, device } from '../Constants';
import { ResetButtonStyles } from '../Helpers';

const ScContainer = styled.section`
  margin-bottom: 101px;

  .swiper {
    margin: 0 -${sizes.gap};
    padding: 0 ${sizes.gap};
  }

  .swiper-slide {
    width: 197px !important;
  }

  .image {
    overflow: hidden;
    width: 197px;
    height: 197px;
    margin-bottom: 14px;

    border-radius: ${radius.radiusSmall};
    object-fit: cover;
    transition: all 0.15s ease-in;
  }

  .link {
    display: block;

    &:hover .image {
      opacity: 0.8;
    }
  }
`;

const ScTopPanel = styled.div`
  display: flex;
  align-items: center;
  gap: 0 39px;
  margin-bottom: 14px;

  .button {
    position: fixed;
    bottom: 59px;
    right: ${sizes.gap};
    z-index: 20;

    @media ${device.tabletBase} {
      position: static;
      margin-left: auto;
    }

    .icon {
      margin-left: 31px;
    }
  }
`;

const ScSwiperNavigation = styled.div`
  display: flex;
  align-items: center;
  gap: 0 10px;
  margin-left: auto;

  @media ${device.tabletBase} {
    margin: 0;
  }

  .swiper-button-next,
  .swiper-button-prev {
    ${ResetButtonStyles}
    position: relative;
    top: unset;
    left: unset;
    right: unset;
    width: 46px;
    height: 46px;
    margin: 0;
    color: ${colors.colorBase};
    background-color: ${colors.colorGrayLight};
    border-radius: 50%;
    transition: all 0.15s ease-in;

    &.swiper-button-disabled {
      opacity: 0.5;
    }

    @media (hover: hover) and (pointer: fine) {
      &:not(.swiper-button-disabled):hover {
        box-shadow: 0px 4px 11px rgba(33, 33, 33, 0.35);
      }
    }

    &:active {
      box-shadow: 0px 4px 11px rgba(33, 33, 33, 0.35);
    }

    &:after {
      font-size: ${fonts.fontSize};
      font-weight: ${fonts.weightBold};
    }
  }
`;

export { ScContainer, ScTopPanel, ScSwiperNavigation };
