import styled, { css } from 'styled-components';
import { sizes, colors, fonts, radius } from '../Constants';

const Link = styled.a`
  color: ${colors.colorBase};
  text-decoration: underline 1px ${colors.colorDarkClarity};
  text-underline-offset: 3px;
  cursor: pointer;
  transition: all 0.15s ease-in;

  &:hover {
    text-decoration-color: ${colors.colorBase};
  }

  ${(p) => p.large && css`
    text-underline-offset: 5px;
    font-size: ${sizes.h4FontSize};
    line-height: ${sizes.h4LineHeight};
  `};

  ${(p) => p.accent && css`
    color: ${(p) => p.theme.colorBrand};
    text-decoration-color: ${(p) => p.theme.colorBrandСlarity};

    &:hover {
      text-decoration-color: ${(p) => p.theme.colorBrand};
    }
  `};

  ${(p) => p.white && css`
    text-decoration-color: transparent;
    color: #fff;

    &:hover {
      text-decoration-color: #fff;
    }
  `};

  ${(p) => p.whiteUnderline && css`
    color: #fff;
    text-decoration-color: ${colors.colorWhiteСlarityLight};

    &:hover {
      text-decoration-color: #fff;
    }
  `};

  ${(p) => p.noUnderline && css`
    text-decoration: none;
  `};

  ${(p) => p.withIcon && css`
    display: flex;
    align-items: center;
  `};

  ${(p) => p.withBage && css`
    position: relative;
  `};
`;

const LinkBadge = styled.div`
  position: absolute;
  top: -11px;
  left: 12px;

  display: flex;
  align-items: center;
  justify-content: center;
  min-width: 24px;
  height: 21px;
  padding: 0 4px;
  box-shadow: 0 0 0 2px ${(p) => p.theme.colorBrandСlarityLight};

  font-size: 12px;
  line-height: 15px;
  font-weight: ${fonts.weightBold};
  
  color: #fff;
  background-color: ${(p) => p.theme.colorBrand};
  border-radius: ${radius.radiusSmaller};
`;

export { Link, LinkBadge };
