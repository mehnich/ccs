import styled, { css } from 'styled-components';
import { sizes } from '../Constants';

const Container = styled.div`
  max-width: ${sizes.desktopLarge};
  width: 100%;
  padding: 0 ${sizes.gap};
  margin: auto;

  ${(p) => p.small && css`
    max-width: ${sizes.desktopBase};
  `};
`;

export default Container;
