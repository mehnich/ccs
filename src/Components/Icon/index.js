import sprite from '../../Img/sprite.svg';

const Icon = ({ className, icon, fill, stroke, width, height }) => (
  <svg className={className} fill={fill} stroke={stroke} width={width} height={height}>
    <use xlinkHref={`${sprite}#${icon}`} />
  </svg>
);

export default Icon;
