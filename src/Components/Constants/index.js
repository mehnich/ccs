export const sizes = {
  mobileSmall: "320px",
  mobileBase: "375px",
  mobileMedium: "425px",
  mobileLarge: "480px",
  tabletSmall: "720px",
  tabletBase: "768px",
  tabletLarge: "1024px",
  desktopSmall: "1140px",
  desktopBase: "1280px",
  desktopLarge: "1440px",

  h1FontSize: "46px",
  h1LineHeight: "56px",
  h2FontSize: "35px",
  h2LineHeight: "42px",
  h3FontSize: "25px",
  h3LineHeight: "30px",
  h4FontSize: "22px",
  h4LineHeight: "27px",
  h5FontSize: "20px",
  h5LineHeight: "24px",

  headerHeight: "58px",
  gap: "24px",
};

export const device = {
  mobileSmall: `(min-width: ${sizes.mobileSmall})`,
  mobileBase: `(min-width: ${sizes.mobileBase})`,
  mobileMedium: `(min-width: ${sizes.mobileMedium})`,
  mobileLarge: `(min-width: ${sizes.mobileLarge})`,
  tabletSmall: `(min-width: ${sizes.tabletSmall})`,
  tabletBase: `(min-width: ${sizes.tabletBase})`,
  tabletLarge: `(min-width: ${sizes.tabletLarge})`,
  desktopSmall: `(min-width: ${sizes.desktopSmall})`,
  desktopBase: `(min-width: ${sizes.desktopBase})`,
  desktopLarge: `(min-width: ${sizes.desktopLarge})`,
};

export const fonts = {
  fontFamily: "'Inter', 'SF UI Display', -apple-system, BlinkMacSystemFont, 'Roboto', 'Droid Sans', 'Helvetica Neue', 'Arial', sans-serif",
  fontSize: "16px",
  lineHeight: "19px",
  weightRegular: "400",
  weightBold: "700",
}

export const colors = {
  colorDark: "#1b1b1b",
  colorBase: "#212121",
  colorBrandDark: "#3143e5",
  colorBrand: "#4859f0",
  colorBrandСlarity: "rgba(72, 89, 240, 0.3)",
  colorBrandСlarityLight: "rgba(72, 89, 240, 0.2)",
  colorGray: "#9b9591",
  colorGrayLight: "#f5f5f5",
  colorGrayDark: "#c4c4c4",
  colorGrayDarker: "#6F6F6F",
  colorGrayLighten: "#e2e0df",
  colorWhiteСlarity: "rgba(255, 255, 255, 0.6)",
  colorWhiteСlarityLight: "rgba(255, 255, 255, 0.4)",
  colorDarkClarity: "rgba(155, 149, 145, 0.5)",
}

export const radius = {
  radiusTiny: "4px",
  radiusSmaller: "10px",
  radiusSmall: "8px",
  radiusBase: "16px",
  radiusMedium: "11px",
  radiusLarge: "25px",
}

export const baseTheme = {
  colorBrand: colors.colorBrand,
  colorBrandDark: colors.colorBrandDark,
  colorBrandСlarity: colors.colorBrandСlarity,
  colorBrandСlarityLight: colors.colorBrandСlarityLight,
};

export const violetTheme = {
  colorBrand: "#bf5bbf",
  colorBrandDark: "#a737a7",
  colorBrandСlarity: "rgba(191, 91, 191, 0.3)",
  colorBrandСlarityLight: "rgba(191, 91, 191, 0.2)",
};
