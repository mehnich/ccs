import styled, { css } from 'styled-components';

const ResetButtonStyles = css`
  padding: 0;
  margin: 0;
  border:0;
  background-color: transparent;
  white-space: nowrap;
  box-shadow: none;
  cursor: pointer;
  appearance: none;

  &:focus {
    outline: none;
  }

  &::-moz-focus-inner {
    border: 0;
  }
`;

const ResetInputStyles = css`
  padding: 0;
  border: none;
  outline: none;
  background-color: transparent;

  &::-moz-focus-inner {
    border: 0;
  }

  &::-webkit-outer-spin-button,
  &::-webkit-inner-spin-button,
  &::-webkit-clear-button {
    -webkit-appearance: none;
    margin: 0;
  }

  &::-webkit-search-decoration,
  &::-webkit-search-cancel-button,
  &::-webkit-search-results-button,
  &::-webkit-search-results-decoration { 
    display: none;
  }

  &[type=email],
  &[type=number],
  &[type=password],
  &[type=search],
  &[type=tel],
  &[type=text],
  &[type=url],
  &[type=radio],
  &[type=checkbox] {
    appearance: none;
  }
`;

const ScreenReadersOnly = styled.div`
  clip: rect(0 0 0 0); 
  clip-path: inset(50%);
  height: 1px;
  overflow: hidden;
  position: absolute;
  white-space: nowrap; 
  width: 1px;
`;

export { ResetButtonStyles, ResetInputStyles, ScreenReadersOnly };
