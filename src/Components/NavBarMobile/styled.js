import styled from 'styled-components';
import { colors, sizes } from '../Constants';
import { Link, LinkBadge } from '../Link';

const ScNavBarMobile = styled.div`
  overflow: auto;
  position: fixed;
  top: ${({ open }) => open ? `${sizes.headerHeight}` : '0'};
  right: 0;
  left: 0;
  z-index: 20;

  display: flex;
  flex-direction: column;
  gap: 30px 0;
  height: calc(100vh - ${sizes.headerHeight});
  padding: ${sizes.gap};

  background-color: ${colors.colorBase};
  transform: ${({ open }) => open ? 'translateY(0)' : 'translateY(-100%)'};
  transition: all 0.15s ease-in-out;
`;

const ScNav = styled.nav`
  display: flex;
  flex-direction: column;
  gap: 16px 0;

  ${Link} {
    width: fit-content;
  }

  ${LinkBadge} {
    top: -14px;
    left: 6px;
  }

  .icon {
    width: 19px;
    height: 19px;
    margin-right: 9px;
  }

  .icon--telephone {
    fill: ${(p) => p.theme.colorBrand};
  }
`;

export { ScNavBarMobile, ScNav };
