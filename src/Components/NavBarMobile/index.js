import { colors } from '../Constants';
import Field from "../Field";
import { Link, LinkBadge } from '../Link';
import { ScNavBarMobile, ScNav } from './styled';
import Icon from '../Icon';

const NavBarMobile = ({ open }) => {
  return (
    <ScNavBarMobile open={open}>
      <Field search type="search" placeholder="Product SKU, Name…" />

      <ScNav>
        <Link href="#" withIcon white noUnderline>
          <Icon icon="list" className="icon" width="25" height="29" fill="#fff" />
          Lists
        </Link>
        <Link href="#" withIcon white noUnderline>
          <Icon icon="account" className="icon" width="30" height="30" stroke="#fff" />
          Account
        </Link>
        <Link href="#" withIcon white noUnderline>
          <Icon icon="quick-order" className="icon icon--quick-order" width="18" height="27" fill="#fff" />
          Quick Order
        </Link>
        <Link href="#" withIcon white noUnderline withBage>
          <LinkBadge>32</LinkBadge>
          <Icon icon="cart" className="icon" width="25" height="25" stroke="#fff" />
          Cart
        </Link>
      </ScNav>

      <ScNav>
        <Link href="#" white noUnderline>Products</Link>
        <Link href="#" white noUnderline>Resources </Link>
        <Link href="#" white noUnderline>Services</Link>
        <Link href="#" white noUnderline>Locations</Link>
        <Link href="#" white noUnderline>Careers</Link>
        <Link href="#" white noUnderline>Support</Link>

        <Link href="#" white noUnderline className="tel" withIcon>
          <Icon icon="telephone" className="icon icon--telephone" width="24" height="24" fill={colors.colorBrand} />
          +(844) 434-672
        </Link>
      </ScNav>
    </ScNavBarMobile>
  )
}

export default NavBarMobile
