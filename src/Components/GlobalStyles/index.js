import { createGlobalStyle } from 'styled-components';
import { sizes, fonts, colors, device } from '../Constants';

const GlobalStyles = createGlobalStyle`
  /* Sticky footer */
  html, body, #root {
    height: 100%;
    scroll-behavior: smooth;
  }

  #root {
    display: flex;
    flex-direction: column;
  }
  /* Sticky footer */

  body {
    overflow-x: hidden;
    min-width: ${sizes.mobileSmall};
    width: 100%;
    padding-top: calc(${sizes.headerHeight} + 12px);
    margin: 0;
    font-family: ${fonts.fontFamily};
    font-size: ${fonts.fontSize};
    line-height: ${fonts.lineHeight};
    font-weight: ${fonts.weightRegular};
    color: ${colors.colorBase};
    background-color: #fff;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;

    @media ${device.tabletLarge} {
      padding-top: 0;
    }

    &.disable-scroll {
      overflow-y: hidden;
    }
  }

  *,
  *:before,
  *:after {
    box-sizing: border-box;
  }

  * {
    -webkit-tap-highlight-color: transparent;
  }

  ul,
  ol {
    list-style: none;
    padding: 0;
  }

  h1,
  h2,
  h3,
  h4,
  h5,
  h6,
  p,
  ul,
  ol,
  li,
  figure,
  figcaption,
  blockquote,
  dl,
  dd {
    font-weight: ${fonts.weightRegular};
    margin: 0;
  }

  img {
    max-width: 100%;
    display: block;
  }

  input,
  button,
  textarea,
  select {
    font: inherit;
  }

  b, strong {
    font-weight: ${fonts.weightBold};
  }
`;

export default GlobalStyles;
