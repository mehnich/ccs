import styled, { css } from 'styled-components';
import { colors, radius } from '../Constants';
import { ResetInputStyles } from '../Helpers';

const ScField = styled.div`
  position: relative;
  display: flex;
  align-items: center;

  ${(p) => p.search && css`
    ${ScInput} {
      padding: 0 16px 0 53px;
    }

    .icon {
      position: absolute;
      top: 50%;
      left: 14px;
      transform: translateY(-50%);
    }
  `};

  ${(p) => p.subscribe && css`
    ${ScInput} {
      height: 57px;
      padding-right: 166px;
      color: ${colors.colorGrayDark};
      background-color: ${colors.colorDark};
      border-color: ${colors.colorDark};
      border-radius: ${radius.radiusMedium};

      &::placeholder {
        color: ${colors.colorGrayDark};
      }

      &:hover,
      &:focus {
        border-color: ${(p) => p.theme.colorBrand};
      }
    }
  `};
`;

const ScInput = styled.input`
  ${ResetInputStyles}
  display: flex;
  align-items: center;
  width: 100%;
  height: 45px; 
  padding: 0 16px;
  border: 1px solid ${colors.colorGrayLight};
  background-color: ${colors.colorGrayLight};
  border-radius: ${radius.radiusSmall};
  transition: all 0.15s ease-in;
  
  &::placeholder {
    color: ${colors.colorGray};
  }

  &:hover,
  &:focus {
    border-color: ${colors.colorGrayLighten};
  }
`;

export { ScField, ScInput };
