import { ScField, ScInput } from './styled';
import Icon from '../Icon';

const Field = ({ search, placeholder, type, subscribe, className }) => {
  return (
    <ScField search={search} subscribe={subscribe} className={className}>
      <ScInput type={type} placeholder={placeholder} />
      {search &&
        <Icon icon="search" className="icon" width="31" height="30" />
      }
    </ScField>
  )
}

export default Field;
