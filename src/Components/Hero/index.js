import { ScContainer, ScGradient, ScInner } from "./styled";
import Text from '../Text';
import Button from '../Button';
import Container from '../Container';

import { EffectFade, Pagination, Autoplay } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import 'swiper/css/pagination';
import 'swiper/css/effect-fade';

import FillImage from '../../Img/fill-1.jpg';
import FillImage2x from '../../Img/fill-1@2x.jpg';
import FillImageWebp from '../../Img/fill-1.webp';
import FillImage2xWebp from '../../Img/fill-1@2x.webp';

const Hero = () => {
  return (
    <ScContainer>
      <Swiper
        modules={[Autoplay, EffectFade, Pagination]}
        spaceBetween={0}
        slidesPerView={1}
        pagination={{clickable: true}}
        effect={"fade"}
        speed={500}
        autoplay={{
          delay: 5000,
          disableOnInteraction: false,
        }}
      >
        <SwiperSlide>
          <ScInner as={Container} small>
            <Text as="h2" className="title" h1 white>Fiberglass and specialty metals are in stock across the United States</Text>
            <Button href="#" as="a" className="button" large noUnderline>Shop Now</Button>
          </ScInner>
          <picture>
            <source srcSet={`${FillImageWebp}, ${FillImage2xWebp} 2x`} type="image/webp" />
            <source srcSet={`${FillImage}, ${FillImage2x} 2x`} type="image/jpg" />
            <img className="image" src={FillImage} srcSet={`${FillImage}, ${FillImage2x} 2x`} width="1392" height="675" alt="fill" />  
          </picture>
          <ScGradient />
        </SwiperSlide>

        <SwiperSlide>
          <ScInner as={Container} small>
            <Text as="h2" className="title" h1 white>Fiberglass and specialty metals are in stock across the Russia</Text>
            <Button href="#" as="a" className="button" large noUnderline>Shop Now</Button>
          </ScInner>
          <picture>
            <source srcSet="https://placehold.co/1392x675/orange/white.webp, https://placehold.co/1392x675/orange/white@2x.webp 2x" type="image/webp" />
            <source srcSet="https://placehold.co/1392x675/orange/white.jpg, https://placehold.co/1392x675/orange/white@2x.jpg 2x" type="image/jpg" />
            <img className="image" src="https://placehold.co/1392x675/orange/white.jpg" srcSet="https://placehold.co/1392x675/orange/white.jpg, https://placehold.co/1392x675/orange/white@2x.jpg 2x" width="1392" height="675" alt="fill" />  
          </picture>
          <ScGradient />
        </SwiperSlide>

        <SwiperSlide>
          <ScInner as={Container} small>
            <Text as="h2" className="title" h1 white>Fiberglass and specialty metals are in stock across the Indonesia</Text>
            <Button href="#" as="a" className="button" large noUnderline>Shop Now</Button>
          </ScInner>
          <picture>
            <source srcSet="https://placehold.co/1392x675/pink/white.webp, https://placehold.co/1392x675/pink/white@2x.webp 2x" type="image/webp" />
            <source srcSet="https://placehold.co/1392x675/pink/white.jpg, https://placehold.co/1392x675/pink/white@2x.jpg 2x" type="image/jpg" />
            <img className="image" src="https://placehold.co/1392x675/pink/white.jpg" srcSet="https://placehold.co/1392x675/pink/white.jpg, https://placehold.co/1392x675/orange/white@2x.jpg 2x" width="1392" height="675" alt="fill" />  
          </picture>
          <ScGradient />
        </SwiperSlide>

        <SwiperSlide>
          <ScInner as={Container} small>
            <Text as="h2" className="title" h1 white>Fiberglass and specialty metals are in stock across the Sri Lanka</Text>
            <Button href="#" as="a" className="button" large noUnderline>Shop Now</Button>
          </ScInner>
          <picture>
            <source srcSet="https://placehold.co/1392x675/violet/white.webp, https://placehold.co/1392x675/violet/white@2x.webp 2x" type="image/webp" />
            <source srcSet="https://placehold.co/1392x675/violet/white.jpg, https://placehold.co/1392x675/violet/white@2x.jpg 2x" type="image/jpg" />
            <img className="image" src="https://placehold.co/1392x675/violet/white.jpg" srcSet="https://placehold.co/1392x675/violet/white.jpg, https://placehold.co/1392x675/orange/white@2x.jpg 2x" width="1392" height="675" alt="fill" />  
          </picture>
          <ScGradient />
        </SwiperSlide>
      </Swiper>
    </ScContainer>
  )
}

export default Hero;
