import styled from 'styled-components';
import { radius, colors, sizes, device } from '../Constants';

const ScContainer = styled.section`
  && {
    overflow: hidden;
    position: relative;
    margin-bottom: 76px;
    border-radius: ${radius.radiusBase};

    .swiper {
      height: 500px;

      @media ${device.tabletBase} {
        height: 675px;
      }
    }

    .swiper-pagination {
      top: unset;
      right: 50%;
      bottom: 21px;
      left: unset;

      display: flex;
      gap: 0 9px;
      width: auto;
      height: 8px;
      transform: translateX(50%);

      @media ${device.tabletBase} {
        right: ${sizes.gap};
        transform: unset;
      }
    }

    .swiper-pagination-bullet {
      margin: unset;
      background-color: ${colors.colorWhiteСlarity};
      border-radius: ${radius.radiusTiny};
      opacity: unset;
      backdrop-filter: blur(5px);
      transition: all 0.15s ease-in;

      &:not(.swiper-pagination-bullet-active):hover {
        background-color: ${colors.colorWhiteСlarityLight};
      }
    }

    .swiper-pagination-bullet-active {
      width: 26px;
      background-color: #fff;
    }
  }

  .image {
    overflow: hidden;

    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;

    width: 100%;
    height: 100%;

    border-radius: ${radius.radiusBase};
    object-fit: cover;
  }
`;

const ScInner = styled.div`
  position: relative;
  z-index: 1;

  .title {
    margin: 50px 0;
    max-width: 582px;
    min-height: 228px;

    @media ${device.tabletBase} {
      margin: 244px 0 54px;
    }
  }

  .button {
    width: 100%;

    @media ${device.mobileLarge} {
      max-width: 320px;
    }
  }
`;

const ScGradient = styled.div`
  position: absolute;
  top: 0;
  left: 0;

  max-width: 1184px;
  width: 100%;
  height: 100%;

  border-radius: ${radius.radiusBase} 0 0 ${radius.radiusBase};
  background: linear-gradient(90.36deg, #1f2670 29.82%, rgba(39, 48, 139, 0) 99.68%);
`;

export { ScContainer, ScInner, ScGradient };
