import styled from 'styled-components';
import { colors, radius, device } from '../Constants';

const ScContainer = styled.section`
  overflow: hidden;
  position: relative;

  margin-bottom: 106px;
  padding: 50px 0;

  border: 1px solid ${colors.colorGrayLighten};
  border-radius: ${radius.radiusBase};

  @media ${device.tabletBase} {
    padding: 131px 0 141px;
  }

  .image {
    position: absolute;
    top: 0;
    right: 0;
    left: auto;

    max-width: 813px;
    width: 100%;
    height: 100%;
    object-fit: cover;
  }
`;

const ScInner = styled.div`
  position: relative;
  z-index: 1;

  .title {
    margin-bottom: 40px;
    max-width: 570px;
  }

  .subtitle {
    margin-bottom: 40px;
    max-width: 570px;
  }

  .button {
    width: 100%;

    @media ${device.mobileLarge} {
      max-width: 320px;
    }
  }
`;

const ScGradient = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  left: auto;

  max-width: 853px;
  width: 100%;
  height: 100%;

  background: linear-gradient(90deg, #fff 7.65%, rgba(255, 255, 255, 0.92) 60.96%, rgba(255, 255, 255, 0) 100%);

  @media ${device.tabletLarge} {
    background: linear-gradient(90deg, #fff 7.65%, rgba(255, 255, 255, 0.92) 23.96%, rgba(255, 255, 255, 0) 73.83%);
  }
`;

export { ScContainer, ScInner, ScGradient };
