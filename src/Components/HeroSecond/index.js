import { ScContainer, ScGradient, ScInner } from "./styled";
import Text from '../Text';
import Button from '../Button';
import Container from '../Container';

import FillImage from '../../Img/fill.jpg';
import FillImage2x from '../../Img/fill@2x.jpg';
import FillImageWebp from '../../Img/fill.webp';
import FillImage2xWebp from '../../Img/fill@2x.webp';

const HeroSecond = () => {
  return (
    <ScContainer>
      <ScInner as={Container} small>
        <Text as="h2" className="title" h2>Large Inventory Of Power Tools & Accessories</Text>
        <Text as="p" className="subtitle" h4>Find the Aluminum and Stainless Steel items for your project in stock and ready to ship today! Avoid project delays and long lead times in a challenging supply chain for specialty metals and non-ferrous items. We are well stocked and here to serve you with guided support.</Text>
        <Button href="#" as="a" className="button" large noUnderline>Shop Now</Button>
      </ScInner>
      <picture>
        <source srcSet={`${FillImageWebp}, ${FillImage2xWebp} 2x`} type="image/webp" />
        <source srcSet={`${FillImage}, ${FillImage2x} 2x`} type="image/jpg" />
        <img className="image" src={FillImage} srcSet={`${FillImage}, ${FillImage2x} 2x`} width="813" height="682" alt="fill" />  
      </picture>
      <ScGradient />
    </ScContainer>
  )
}

export default HeroSecond;
