import styled, { css } from 'styled-components';
import { sizes, colors, device, fonts } from '../Constants';

const Text = styled.div`
  ${(p) => p.h1 && css`
    font-size: ${sizes.h2FontSize};
    line-height: ${sizes.h2LineHeight};
    font-weight: ${fonts.weightBold};

    @media ${device.tabletSmall} {
      font-size: ${sizes.h1FontSize};
      line-height: ${sizes.h1LineHeight};
    }
  `};

  ${(p) => p.h2 && css`
    font-size: ${sizes.h2FontSize};
    line-height: ${sizes.h2LineHeight};
    font-weight: ${fonts.weightBold};
  `};

  ${(p) => p.h3 && css`
    font-size: ${sizes.h3FontSize};
    line-height: ${sizes.h3LineHeight};
    font-weight: ${fonts.weightBold};
  `};

  ${(p) => p.h4 && css`
    font-size: ${sizes.h4FontSize};
    line-height: ${sizes.h4LineHeight};
  `};

  ${(p) => p.h5 && css`
    font-size: ${sizes.h5FontSize};
    line-height: ${sizes.h5LineHeight};
  `};

  ${(p) => p.bold && css`
    font-weight: ${fonts.weightBold};
  `};

  ${(p) => p.gray && css`
    color: ${colors.colorGrayDarker};
  `};

  ${(p) => p.white && css`
    color: #fff;
  `};
`;

export default Text;
