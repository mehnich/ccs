import { colors, device } from '../Constants';
import { ScHeader, ScHeaderInner, ScTopPanel, ScCenterPanel, ScNav, ScBottomPanel } from './styled';
import Field from "../Field";
import { Link, LinkBadge } from '../Link';
import useMediaQuery from '../../Hooks';
import BurgerMenu from '../BurgerMenu';
import Icon from '../Icon';
import { ScreenReadersOnly } from '../Helpers';

const Header = () => {
  const isTabletLarge = useMediaQuery(device.tabletLarge);

  return (
    <ScHeader>
      <ScHeaderInner>
        {isTabletLarge &&
          <ScTopPanel>
            <Link href="#" accent>Request a Sample</Link>
            <Link href="#" accent>Help</Link>
            <Link href="#" accent>Contact</Link>
          </ScTopPanel>
        }

        <ScCenterPanel>
          <Link href="#" className="site-logo">
            <Icon icon="logo" width="103" height="29" fill={colors.colorBase} />
            <ScreenReadersOnly as="span">Logo</ScreenReadersOnly>
          </Link>

          {isTabletLarge &&
            <>
              <Field className="site-search" search type="search" placeholder="Product SKU, Name…" />

              <ScNav>
                <Link href="#" withIcon noUnderline>
                  <Icon icon="list" className="icon icon--list" width="25" height="29" fill={colors.colorGray} />
                  Lists
                </Link>
                <Link href="#" withIcon noUnderline>
                  <Icon icon="account" className="icon icon--account" width="30" height="30" stroke={colors.colorGray} />
                  Account
                </Link>
                <Link href="#" withIcon noUnderline>
                  <Icon icon="quick-order" className="icon icon--quick-order" width="18" height="27" fill={colors.colorGray} />
                  Quick Order
                </Link>
                <Link href="#" withIcon noUnderline withBage>
                  <LinkBadge>32</LinkBadge>
                  <Icon icon="cart" className="icon icon--cart" width="25" height="25" stroke={colors.colorGray} />
                  Cart
                </Link>
              </ScNav>
            </>
          }
        </ScCenterPanel>
        
        {isTabletLarge &&
          <ScBottomPanel>
            <Link href="#" large>Products</Link>
            <Link href="#" large>Resources </Link>
            <Link href="#" large>Services</Link>
            <Link href="#" large>Locations</Link>
            <Link href="#" large>Careers</Link>
            <Link href="#" large>Support</Link>

            <Link href="#" className="tel" withIcon accent large>
              <Icon icon="telephone" className="tel__icon" width="24" height="24" fill={colors.colorBrand} />
              +(844) 434-672
            </Link>
          </ScBottomPanel>
        }
      </ScHeaderInner>

      {!isTabletLarge &&
        <BurgerMenu />
      }
    </ScHeader>
  )
}

export default Header;
