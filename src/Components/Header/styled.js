import styled from 'styled-components';
import { colors, device, sizes } from '../Constants';
import { Link } from '../Link';

const ScHeader = styled.header`
  position: fixed;
  top: 0;
  right: 0;
  left: 0;
  z-index: 30;
  
  @media ${device.tabletLarge} {
    position: static;
  }
`;

const ScHeaderInner = styled.div`
  position: relative;
  z-index: 30;
  
  display: flex;
  align-items: center;
  height: ${sizes.headerHeight};
  padding: 0 ${sizes.gap};
  background-color: #fff;

  @media ${device.tabletLarge} {
    display: block;
    height: auto;
    padding: 12px 0 23px;
  }

  ${Link} {
    white-space: nowrap;
  }
`;

const ScTopPanel = styled.nav`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  gap: 0 18px;
  margin-bottom: 10px;
`;

const ScCenterPanel = styled.div`
  display: flex;
  align-items: center;
  gap: 0 19px;

  @media ${device.tabletLarge} {
    margin-bottom: 26px;
  }

  .site-search {
    max-width: 479px;
    width: 100%;
  }

  .site-logo {
    flex-shrink: 0;
    margin-right: 40px;
  }
`;

const ScField = styled.div`
  width: 200px;
  height: 45px;
  background-color: ${colors.colorGrayLight};
`;

const ScNav = styled.nav`
  display: flex;
  align-items: center;
  gap: 0 19px;
  margin-left: auto;

  ${Link} {
    height: 30px;

    .icon {
      margin-right: 11px;
      transition: all 0.15s ease-in;
    }

    &:hover{
      .icon--list,
      .icon--quick-order {
        fill: ${colors.colorBase};
      }

      .icon--account,
      .icon--cart {
        stroke: ${colors.colorBase};
      }
    } 
  }
`;

const ScBottomPanel = styled.nav`
  display: flex;
  align-items: center;
  gap: 0 19px;

  .tel {
    margin-left: auto;
  }

  .tel__icon {
    flex-shrink: 0;
    margin-right: 9px;
    fill: ${(p) => p.theme.colorBrand};
  }
`;

export { ScHeader, ScHeaderInner, ScTopPanel, ScCenterPanel, ScNav, ScField, ScBottomPanel };
