import styled from 'styled-components';
import { colors, radius, sizes } from '../Constants';
import { ResetButtonStyles } from '../Helpers';

const ScBurger = styled.button`
  ${ResetButtonStyles}
  position: absolute;
  top: 50%;
  right: ${sizes.gap};
  z-index: 30;

  display: flex;
  justify-content: space-around;
  flex-direction: column;
  width: 30px;
  height: 30px;
  margin-top: -15px;
  
  div {
    width: 100%;
    height: 3px;
    background-color: ${colors.colorBase};
    border-radius: ${radius.radiusSmall};
    transform-origin: 1px;
    transition: all 0.3s linear;

    &:nth-child(1) {
      transform: ${({ open }) => open ? 'rotate(45deg)' : 'rotate(0)'};
    }

    &:nth-child(2) {
      opacity: ${({ open }) => open ? 0 : 1};
    }

    &:nth-child(3) {
      transform: ${({ open }) => open ? 'rotate(-45deg)' : 'rotate(0)'};
    }
  }
`;

export default ScBurger;
