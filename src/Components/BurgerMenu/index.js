import React, { useState } from 'react';
import NavBarMobile from '../NavBarMobile';
import ScBurger from './styled';

const BurgerMenu = () => {
  const [open, setOpen] = useState(false);

  if (open) document.body.classList.add('disable-scroll');
  else document.body.classList.remove('disable-scroll');
  
  return (
    <>
      <ScBurger open={open} onClick={() => setOpen(!open)}>
        <div />
        <div />
        <div />
      </ScBurger>
      <NavBarMobile open={open} />
    </>
  )
}

export default BurgerMenu;
