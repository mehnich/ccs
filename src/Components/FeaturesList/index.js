import { ScList, ScListItem } from "./styled";
import { Link } from '../Link';
import Text from '../Text';
import Icon from '../Icon';
import { colors } from "../Constants";

const FeaturesList = () => {
  return (
    <ScList>
      <ScListItem>
        <Link href="#" noUnderline>
          <Icon icon="catalog" className="icon" width="77" height="74" fill={colors.colorBrand} />
          <Text h3>Products Catalog</Text>
        </Link>
      </ScListItem>

      <ScListItem>
        <Link href="#" noUnderline>
          <Icon icon="support" className="icon" width="86" height="82" stroke={colors.colorBrand} />
          <Text h3>24/7 Customer Support</Text>
        </Link>
      </ScListItem>

      <ScListItem>
        <Link href="#" noUnderline>
          <Icon icon="product" className="icon" width="57" height="76" fill={colors.colorBrand} />
          <Text h3>Product Ideas</Text>
        </Link>
      </ScListItem>
    </ScList>
  )
}

export default FeaturesList;
