import styled from 'styled-components';
import { sizes, device } from '../Constants';
import { Link } from '../Link';

const ScList = styled.ul`
  display: grid;
  justify-content: center;
  grid-gap: 30px 0;
  max-width: ${sizes.desktopSmall};
  margin: 0 auto 112px;

  @media ${device.tabletBase} {
    grid-template-columns: 1fr 1fr 1fr;
    grid-gap: 0 16px;
  }
`;

const ScListItem = styled.li`
  display: flex;
  justify-content: center;

  ${Link} {
    display: inline-flex;
    flex-direction: column;
    align-items: center;
    text-align: center;
    text-decoration-color: transparent;

    &:hover,
    &:focus {
      .icon {
        opacity: 0.8;
      }
    }
  }

  .icon {
    height: 74px;
    margin-bottom: 28px;
    fill: ${(p) => p.theme.colorBrand};
    stroke: ${(p) => p.theme.colorBrand};
    transition: all 0.15s ease-in;
  }
`;

export { ScList, ScListItem };
