import { colors } from '../Constants';
import { ScFooter, ScTopPanel, ScNav, ScList, ScSubscribe, ScSubscribeForm, ScBottomPanel } from "./styled";
import Field from "../Field";
import Text from "../Text";
import Button from "../Button";
import { Link } from '../Link';

import Icon from '../Icon';
import Payments from '../../Img/payments.png';
import Payments2x from '../../Img/payments@2x.png';
import PaymentsWebp from '../../Img/payments.webp';
import Payments2xWebp from '../../Img/payments@2x.webp';

const Footer = () => {
  const today = new Date();
  const year = today.getFullYear();

  return (
    <ScFooter>
      <ScTopPanel>
        <ScNav>
          <Text as="h3" h5 bold gray>Helpful Links</Text>
          <ScList>
            <Text as="li">
              <Link href="#" white>Buyer’s Guides</Link>
            </Text>
            <Text as="li">
              <Link href="#" white>Calculators</Link>
            </Text>
            <Text as="li">
              <Link href="#" white>All Brands</Link>
            </Text>
            <Text as="li">
              <Link href="#" white>Catalg Updates</Link>
            </Text>
          </ScList>
        </ScNav>

        <ScNav>
          <Text as="h3" h5 bold gray>Customer Services</Text>
          <ScList>
            <Text as="li">
              <Link href="#" white>Express Delivery</Link>
            </Text>
            <Text as="li">
              <Link href="#" white>Privacy Statement</Link>
            </Text>
            <Text as="li">
              <Link href="#" white>Terms of Use</Link>
            </Text>
            <Text as="li">
              <Link href="#" white>Pricing Policy</Link>
            </Text>
            <Text as="li">
              <Link href="#" white>Returns and Refunds</Link>
            </Text>
            <Text as="li">
              <Link href="#" white>Help and FAQ</Link>
            </Text>
            <Text as="li">
              <Link href="#" white>Return Points</Link>
            </Text>
            <Text as="li">
              <Link href="#" white>Returns and Orders</Link>
            </Text>
          </ScList>
        </ScNav>
          
        <ScNav>
          <Text as="h3" h5 bold gray>Support</Text>
          <ScList>
            <Text as="li">
              <Link href="#" white>Contact Us</Link>
            </Text>
            <Text as="li">
              <Link href="#" white>FAQ</Link>
            </Text>
            <Text as="li">
              <Link href="#" white>Feedback</Link>
            </Text>
            <Text as="li">
              <Link href="#" white>Live Chat</Link>
            </Text>
            <Text as="li">
              <Link href="#" white>My Account</Link>
            </Text>
            <Text as="li" className="tel">
              <Link href="#" white withIcon>
                <Icon icon="telephone" className="tel__icon" width="18" height="18" fill={colors.colorBrand} />
                +(844) 434-672
              </Link>
            </Text>
          </ScList>
        </ScNav>

        <ScSubscribe>
          <Text as="p" bold gray>Subscribe to our mailing list to receive exclusive offers and the latest product updates</Text>
          <ScSubscribeForm>
            <Field subscribe type="email" placeholder="Your E-mail" />
            <Button type="button">Subscribe</Button>
          </ScSubscribeForm>
          <picture>
            <source srcSet={`${PaymentsWebp}, ${Payments2xWebp} 2x`} type="image/webp" />
            <source srcSet={`${Payments}, ${Payments2x} 2x`} type="image/png" />
            <img src={Payments} srcSet={`${Payments}, ${Payments2x} 2x`} width="81" height="48" alt="methods" />  
          </picture>
        </ScSubscribe>
      </ScTopPanel>  

      <ScBottomPanel>
        <Text as="p" gray>© «Brand» {year}, All Rights Reserved</Text>
      </ScBottomPanel>
    </ScFooter>
  )
}

export default Footer;
