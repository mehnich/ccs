import styled from 'styled-components';
import { colors, radius, sizes, device } from '../Constants';
import Text from '../Text';
import Button from '../Button';

const ScFooter = styled.footer`
  padding: 37px ${sizes.gap} 13px;
  margin: 0 -${sizes.gap};
  background-color: ${colors.colorBase};
  border-radius: ${radius.radiusBase} ${radius.radiusBase} 0 0;

  @media ${device.tabletLarge} {
    margin: 0;
  }
`;

const ScTopPanel = styled.div`
  display: grid;
  
  grid-gap: 30px 0;
  margin-bottom: 28px;

  @media ${device.tabletLarge} {
    grid-template-columns: 1fr 1fr 1fr minmax(300px, 464px);
    grid-gap: 0 ${sizes.gap};
  }
`;

const ScNav = styled.section`
  ${Text} {
    margin-bottom: 23px;
    white-space: nowrap;
  }
`;

const ScList = styled.ul`
  ${Text} {
    margin-bottom: 5px;

    &:last-child {
      margin-bottom: 0;
    }

    &.tel {
      margin-top: 14px;

      .tel__icon {
        margin-right: 7px;
        fill: ${(p) => p.theme.colorBrand};
      }
    }
  }
`;

const ScSubscribe = styled.div`
  max-width: 464px;

  @media ${device.tabletLarge} {
    max-width: unset;
  }

  ${Text} {
    margin-bottom: 21px;
  }
`;

const ScSubscribeForm = styled.form`
  position: relative;
  margin-bottom: 47px;

  ${Button} {
    position: absolute;
    top: 50%;
    right: 3px;
    width: 147px;
    transform: translateY(-50%);
  }
`;

const ScBottomPanel = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;

  ${Text} {
    color: ${colors.colorGray};
  }
`;

export { ScFooter, ScTopPanel, ScNav, ScList, ScSubscribe, ScSubscribeForm, ScBottomPanel };
