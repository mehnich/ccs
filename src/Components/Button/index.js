import styled, { css } from 'styled-components';
import { fonts, sizes, radius } from '../Constants';
import { ResetButtonStyles } from '../Helpers';

const Button = styled.button`
  ${ResetButtonStyles}
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  height: 50px;
  padding: 0 16px;

  font-weight: ${fonts.weightBold};
  color: #fff;
  border-radius: ${radius.radiusSmall};
  background-color: ${(p) => p.theme.colorBrand};
  transition: all 0.15s ease-in;

  @media (hover: hover) and (pointer: fine) {
    &:hover {
      background-color: ${(p) => p.theme.colorBrandDark};
    }
  }

  &:active {
    background-color: ${(p) => p.theme.colorBrand};
  }

  ${(p) => p.large && css`
    height: 65px;
    font-size: ${sizes.h4FontSize};
    border-radius: ${radius.radiusBase};
  `};

  ${(p) => p.rounded && css`
    padding: 0 ${sizes.gap};
    border-radius: ${radius.radiusLarge};
    box-shadow: 0px 4px 11px rgba(33, 33, 33, 0.35);
  `};

  ${(p) => p.noUnderline && css`
    text-decoration: none;
  `};
`;

export default Button;
